var express = require('express');
var http = require('http');
var fs = require('fs');
var bodyParser = require('body-parser');
var pdf = require('html-pdf');
var nunjucks = require('nunjucks');
const app = express();

nunjucks.configure('templates', {
    autoescape: true,
    express: app
});

const phantonConfig = {
    // Base path that's used to load files (images, css, js) when they aren't
    // referenced using a host
    base: "file://" + __dirname + "templates/css"
};

app.get('/api/pdf', function (req, res) {
    var obj = {
        text: 'Hello world'
    };

    var renderedHtml = nunjucks.render('example.html', obj);
    pdf
        .create(renderedHtml, phantonConfig)
        .toStream(function (err, stream) {
            stream.pipe(res);
        });
});

app.listen(8000, () => {
    console.log('Server running and listenning to %s', 8000);
});